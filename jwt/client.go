package jwt

import (
	"github.com/go-chi/jwtauth/v5"
)

type UserReq struct {
	Username string `json:"username" example:"admin"`
	Password string `json:"password" example:"pass"`
}

var UsersDB = map[string]string{} // map[string]string

var TokenAuth = jwtauth.New("HS256", []byte("secret"), nil)

package jwt

import (
	"encoding/json"
	"golang.org/x/crypto/bcrypt"
	"net/http"
)

// RegistrationHandler - register new user
// @summary 		Регистрация нового пользователя
// @description 	Регистрация нового пользователя.
// @Tags 			auth
// @Accept 			json
// @Produce 		json
// @Param 			request body jwt.UserReq true "User request"
// @success 200 {string} string "User registered"
// @Failure 400 {string} string "Bad request"
// @Router /api/register [post]
func RegistrationHandler(w http.ResponseWriter, r *http.Request) {

	userReq := UserReq{}

	err := json.NewDecoder(r.Body).Decode(&userReq)
	if err != nil {
		http.Error(w, "Error decoding request body: "+err.Error(), http.StatusBadRequest)
		return
	}

	// проверка что пользователь существует
	if _, ok := UsersDB[userReq.Username]; ok {
		http.Error(w, "User already exists", http.StatusBadRequest)
		return
	}

	hash, _ := bcrypt.GenerateFromPassword([]byte(userReq.Password), bcrypt.DefaultCost)

	UsersDB[userReq.Username] = string(hash)

	w.WriteHeader(http.StatusOK)
	w.Write([]byte("User registered"))
}

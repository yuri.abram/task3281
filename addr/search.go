package addr

import (
	"encoding/json"
	"net/http"
)

type SearchRequest struct {
	Query string `json:"query"`
}

type SearchResponse struct {
	Addresses []*Address `json:"suggestions"`
}

// Search
// @Summary 		Поиск адреса
// @Description Поиск адреса
// @ID 			search
// @Tags 		geocode
// @Accept 		json
// @Produce 	json
// @Security 	Bearer
// @Param 		request body addr.SearchRequest true "Search request"
// @Success 	200 	{object} 	addr.SearchResponse
// @Failure 	400		{string}	string "Bad request"
// @Failure 	401		{string}	string "Unauthorized"
// @Failure 	500		{string}	string "Internal server error"
// @Router		/api/address/search [post]
func Search(w http.ResponseWriter, r *http.Request) {
	// check for POST method
	if r.Method != http.MethodPost {
		http.Error(w, "Bad request", http.StatusBadRequest)
		return
	}
	var sr SearchRequest
	err := json.NewDecoder(r.Body).Decode(&sr)
	if err != nil {
		http.Error(w, "Error decoding request body: "+err.Error(), http.StatusBadRequest)
		return
	}

	resp, err := doRequest(&sr)
	if err != nil {
		http.Error(w, "Error executing request: "+err.Error(), http.StatusInternalServerError)
		return
	}

	var searchResponse *SearchResponse
	err = json.NewDecoder(resp.Body).Decode(&searchResponse)
	if err != nil {
		http.Error(w, "Error decoding response body: "+err.Error(), http.StatusInternalServerError)
		return
	}

	jsonBytes, err := json.Marshal(searchResponse)
	if err != nil {
		http.Error(w, "Error marshalling response: "+err.Error(), http.StatusInternalServerError)
		return
	}

	w.WriteHeader(http.StatusOK)
	w.Header().Set("Content-Type", "application/json")
	w.Write(jsonBytes)
}

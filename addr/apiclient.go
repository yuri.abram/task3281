package addr

import (
	"bytes"
	"encoding/json"
	"fmt"
	"net/http"
)

const (
	geolocateURL = "http://suggestions.dadata.ru/suggestions/api/4_1/rs/geolocate/address"
	suggestURL   = "http://suggestions.dadata.ru/suggestions/api/4_1/rs/suggest/address"
	token        = "Token 08f266e9aceeebd38c7a744dacb7b9c9923c5264"
)

type Address struct {
	Value string `json:"value"`
}

func doRequest(reqType interface{}) (*http.Response, error) {

	apiURL, jsonBody, err := apiSelect(reqType)
	if err != nil {
		return nil, fmt.Errorf("bad request: %w", err)
	}

	req, err := http.NewRequest(
		"POST",
		apiURL,
		bytes.NewBuffer(jsonBody))

	if err != nil {
		return nil, fmt.Errorf("error creating request: %w", err)
	}

	req.Header.Set("Content-Type", "application/json")
	req.Header.Set("Accept", "application/json")
	req.Header.Set("Authorization", token)

	client := http.Client{}

	resp, err := client.Do(req)
	if err != nil {
		return nil, fmt.Errorf("error executing request: %w", err)
	}
	return resp, nil
}

func apiSelect(reqType interface{}) (apiURL string, jsonBody []byte, err error) {
	switch reqType.(type) {
	case *SearchRequest:
		apiURL = suggestURL
		jsonBody, err = json.Marshal(reqType)
		if err != nil {
			return "", nil, fmt.Errorf("bad request, error marshalling request body: %w", err)
		}
		return apiURL, jsonBody, nil
	case *GeocodeRequest:
		jsonBody, err = json.Marshal(reqType)
		if err != nil {
			return "", nil, fmt.Errorf("bad request, error marshalling request body: %w", err)
		}
		apiURL = geolocateURL
		return apiURL, jsonBody, nil
	default:
		return "", nil, fmt.Errorf("bad request")
	}
}

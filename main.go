package main

import (
	"context"
	"github.com/go-chi/chi"
	"github.com/go-chi/chi/middleware"
	"github.com/go-chi/jwtauth/v5"
	httpSwagger "github.com/swaggo/http-swagger"
	"gitlab/task3281/addr"
	_ "gitlab/task3281/docs"
	"gitlab/task3281/jwt"
	"log"
	"net/http"
	"os"
	"os/signal"
	"syscall"
	"time"
)

// @title задача 1.6.3 & задача 1.7.4
// @version 2.0
// @description  Геосервис API с авторизацией
// @host localhost:8080
// @BasePath /

//	@securityDefinitions.apikey	Bearer
//	@in							header
//	@name						Authorization
//	@description The following syntax must be used in the 'Authorization' header: Bearer: xxxxxx.yyyyyyy.zzzzzz

func main() {
	r := chi.NewRouter()

	r.Use(middleware.Logger)

	// Protected routes
	r.Group(func(r chi.Router) {
		r.Use(jwtauth.Verifier(jwt.TokenAuth))
		r.Use(jwtauth.Authenticator(jwt.TokenAuth))

		r.Post("/api/address/search", addr.Search)   //search.go
		r.Post("/api/address/geocode", addr.Geocode) //geocode.go
	})

	// Public routes
	r.Group(func(r chi.Router) {

		r.Get("/swagger/*", httpSwagger.Handler(
			httpSwagger.URL("http://localhost:8080/swagger/doc.json"),
		))

		r.Post("/api/login", jwt.LoginHandler)
		r.Post("/api/register", jwt.RegistrationHandler)

	})

	// Создание HTTP-сервера
	server := &http.Server{
		Addr:         ":8080",
		Handler:      r,
		ReadTimeout:  10 * time.Second,
		WriteTimeout: 10 * time.Second,
	}
	// Создание канала для получения сигналов остановки
	shutdown := make(chan os.Signal, 1)
	signal.Notify(shutdown, os.Interrupt, syscall.SIGTERM)

	// Запуск сервера в отдельной горутине
	go func() {
		log.Println("Starting server...")
		log.Println("Swagger: http://localhost:8080/swagger/")
		if err := server.ListenAndServe(); err != nil && err != http.ErrServerClosed {
			log.Fatalf("Server error: %v", err)
		}
	}()

	// Ожидание сигнала остановки
	<-shutdown

	// Создание контекста с таймаутом для graceful shutdown
	ctx, cancel := context.WithTimeout(context.Background(), 5*time.Second)
	defer cancel()

	// Остановка сервера с использованием graceful shutdown
	log.Println("Stopping server...")
	server.Shutdown(ctx)

	log.Println("Server stopped gracefully")
}
